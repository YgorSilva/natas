<?php

function bruteforce_xor_encrypted_char($chr, $enc) {
  $chrval = 1;
  while ($chr != ($enc ^ chr($chrval))) $chrval++;
  return chr($chrval);
}

function bruteforce_xor_encrypted($str, $enc) {
  $key = '';
  for ($i = 0; $i < strlen($str); $i++) $key .= bruteforce_xor_encrypted_char($str[$i], $enc[$i]);
  return $key;
}

function extract_repeated($str) {
  for ($i = 1; $i <= intval(strlen($str) / 2); $i++) {
    $candidate = substr($str, 0, $i);
    $check = substr($str, $i, $i);
    if ($candidate == $check) return $candidate;
  }
  return $str;
}

function get_key() {
  $defaultdata = array( "showpassword"=>"no", "bgcolor"=>"#ffffff");
  $str = json_encode($defaultdata);
  $enc = base64_decode('MGw7JCQ5OC04PT8jOSpqdmkgJ25nbCorKCEkIzlscm5oKC4qLSgubjY%3D');
  $repeating = bruteforce_xor_encrypted($str, $enc);
  return extract_repeated($repeating);
}

function xor_encrypt($in) {
  $key = get_key();
  $text = $in;
  $outText = '';

  // Iterate through each character
  for($i=0;$i<strlen($text);$i++) {
  $outText .= $text[$i] ^ $key[$i % strlen($key)];
  }

  return $outText;
}

function get_payload() {
  $str = json_encode(array("showpassword"=>"yes", "bgcolor"=>"#ffffff"));
  return base64_encode(xor_encrypt($str));
}

echo get_key() . "\n";
echo get_payload();